## How this file is used ##
# docker build .
# docker tag <tag> poffey21/python-cfcli:3.6.2
# docker push poffey21/python-cfcli:3.6.2

FROM python:3.6.2

RUN apt-get -qqy update && apt-get install -qqy --no-install-recommends apt-utils apt-transport-https curl

RUN wget -q -O - https://packages.cloudfoundry.org/debian/cli.cloudfoundry.org.key | apt-key add - \
  && echo "deb http://packages.cloudfoundry.org/debian stable main" | tee /etc/apt/sources.list.d/cloudfoundry-cli.list \
  && apt-get -qqy update \
  && apt-get -qqy install cf-cli \
  && rm /etc/apt/sources.list.d/cloudfoundry-cli.list \
  && rm -rf /var/lib/apt/lists/* /var/cahe/apt/*

RUN cf --version
