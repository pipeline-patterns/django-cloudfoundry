"""splash URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib.auth.views import login
from django.contrib.auth.views import logout

from . import views

urlpatterns = [
    url(r'^$', views.WelcomeTemplateView.as_view(), name='welcome'),
    url(r'^signin/$', login, {'template_name': 'splash/signin.html'}, name='signin'),
    url(r'^signout/$', logout, name='signout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^profile/$', views.ProfileTemplateView.as_view(), name='profile'),
]
