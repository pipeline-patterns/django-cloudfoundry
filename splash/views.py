from django.shortcuts import render
from django.views import generic


class ProfileTemplateView(generic.TemplateView):
    template_name = 'splash/profile.html'


class WelcomeTemplateView(generic.TemplateView):
    template_name = 'splash/index.html'


def signup(request):
    signup_form = None
    return render(request, 'splash/signup.html', {
        'form': signup_form
    })
