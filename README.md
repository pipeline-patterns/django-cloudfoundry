## Getting Started

Cloud Foundry is a popular PaaS solution for Organizations to adopt. GitLab is
also an excellent choice for organizations.  Let's see how they work together.

This demo will use freely-available services.

also, all services used can be run locally. 

### Accounts Needed

* [Pivotal Web Services](https://login.run.pivotal.io/login)
* [Docker](https://www.docker.com/)
* [GitLab](https://gitlab.com/)

### Tools/Platforms Used

* Git
* cf CLI
* Docker CE
* Python/Django
* GitLab Runner (docker)
  * Technically I used Windows --> VirtualBox --> CentOS (with [Docker installed](https://docs.docker.com/engine/installation/linux/docker-ce/centos/#install-docker-ce)), [installing](https://docs.gitlab.com/runner/install/linux-repository.html) and [registering](https://docs.gitlab.com/runner/register/index.html) my runner here.

### Configurations Required

* Register computer as GitLab Runner
* Lock all CI/CD Pipelines to the aforementioned GitLab Runner
* GitLab Secret Variables


### Directories and Files

* `.venv/`: Python Virtual Environment for local development only
* `splash/`: Django Application
* `templates/`: Django Web Page Templates shared across all Applications within this project
* `www`: Django Project files
* `.gitignore`: Git client file to know what to work with
* `.gitlab-ci.yml`: GitLab CI/CD Workflow
* `manage.py`: Django script
* `manifest.yml`: Cloud Foundry
* `Procfile`: Cloud Foundry
* `README.md`: GitLab initial page
* `requirements.txt`: Cloud Foundry uses this to install necessary packages
* `runtime.txt`: Cloud Foundry reads this to know what buildpack to use
* `uwsgi.ini`: Python uses this to run the application


### Bonus Material - How I like starting a Django Project & App

_Do not open your IDE yet_

* Create Git Repository in GitLab
* Clone the Repository `git clone git@gitlab.com:pipeline-patterns/django-cloudfoundry.git`
* Create and Activate Virtual Environment: `virtualenv django-cloudfoundry/.venv`

I like to name my projects www so that it's in the root of the git project and 
so it's always on the bottom.

```Powershell
django-admin.exe startproject www
move www www2
move www2\* .\
del www2
django-admin startapp splash
```

Now you may open your IDE. Why wait?

* PyCharm now auto-detects your Virtual Environment
* Your git project is already initiated

